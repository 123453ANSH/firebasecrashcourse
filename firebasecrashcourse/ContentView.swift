//
//  ContentView.swift
//  firebasecrashcourse
//
//  Created by Ansh Verma on 6/19/20.
//  Copyright © 2020 Ansh Verma. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, World!")
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
